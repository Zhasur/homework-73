const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 5000;

/// Task-3

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});

/// Task-2

app.get('/encode/:text', (req, res) => {
    const cipher = Vigenere.Cipher('password').crypt(req.params.text);
    res.send(cipher);
});

app.get('/decode/:text', (req, res) => {
    const decipher = Vigenere.Decipher('password').crypt(req.params.text);
    res.send(decipher);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});